#!/bin/bash

# Files that must be copied into the roaming profile directory.

ROAMING_PROFILE_FILES=".bash_profile .minttyrc"

# Locate the base directory of the repository.

THIS_SCRIPT=$(readlink -f $0)
BASE_DIR=$(dirname $THIS_SCRIPT)

# Work out which OS and terminal is being used.

case $(cat /proc/version 2>/dev/null) in
  MSYS*)                     SHELL_ENVIRONMENT="gitbash" ;;
  *Chromium\ OS*)            SHELL_ENVIRONMENT="chromeos" ;;
  *microsoft-standard-WSL2*) SHELL_ENVIRONMENT="wsl" ;;
esac

# Work out if a Windows roaming profile is being used.

if [ "${SHELL_ENVIRONMENT}" = "gitbash" ]
then

  PROFILEDRIVE=`echo $USERPROFILE | cut -d'\' -f1`

  if [ "$PROFILEDRIVE" != "$HOMEDRIVE" ]
  then

    ROAMING_HOME=`cygpath $HOMEDRIVE$HOMEPATH`
    export HOME=`cygpath $USERPROFILE`

    # Make sure the minimum set of files appear in the roaming home directory.

    for FILE in $ROAMING_PROFILE_FILES
    do
      cp $BASE_DIR/$FILE $ROAMING_HOME/$FILE
    done

  fi
fi

# Work out if we are on a work machine.

case $SHELL_ENVIRONMENT in

  chromeos|wsl)
    FQDN=`hostname -d`
    case $FQDN in
      is.ed.ac.uk) ATWORK=1 ;;
    esac
    ;;

  gitbash)
    DOMAIN=`powershell -Command 'Get-ChildItem -Path Env:\UserDomain | Select-Object -ExpandProperty Value'`
    case $DOMAIN in
      ED) ATWORK=1 ;;
    esac
    ;;

esac

# Copy the git config file, extracting and replacing the user name and email.

if [ -r $HOME/.gitconfig ]
then
  TEMP_USER_SECTION=`mktemp`
  awk -f $BASE_DIR/etc/extract-user-section.awk $HOME/.gitconfig > $TEMP_USER_SECTION
  cp $BASE_DIR/.gitconfig $HOME/.gitconfig
  cat $TEMP_USER_SECTION >> $HOME/.gitconfig
  rm -f $TEMP_USER_SECTION
else
  cp $BASE_DIR/.gitconfig $HOME/.gitconfig
fi

# Copy the SSH config file.

if [ "${SHELL_ENVIRONMENT}" = "gitbash" -a "x${ROAMING_HOME}" != "x" ]
then
  SSH_DIR=$ROAMING_HOME/.ssh
else
  SSH_DIR=$HOME/.ssh
fi

# Make sure that the SSH directory is secure.

mkdir -p $SSH_DIR
chmod go-rwx $SSH_DIR

# Choose which SSH config file to copy based on whether the update script is run
# at work or at a remote location.

if [ "$ATWORK" != "" ]
then
  cp $BASE_DIR/ssh/config.atwork $SSH_DIR/config
else
  cp $BASE_DIR/ssh/config.remote $SSH_DIR/config
fi

# Create a separate UUN config file if one does not exist. This is used to
# define the user's UUN outside of the main SSH config file.

if [ ! -r "${SSH_DIR}/config.uun" ]
then
  echo "User UUN" > $SSH_DIR/config.uun
fi

# Create any directories that are needed.

for DIR in $(cd $BASE_DIR; find . -type d -not -path "." -not -path "./.git" -not -path "./.git/*")
do
  if [ ! -d "${HOME}/${DIR}" ]
  then
    mkdir $HOME/$DIR
  fi
done

# Copy the rest of the dotfiles.

for ITEM in $(cd $BASE_DIR; find . -type f -not -path "./.git/*" -not -path "./ssh/*")
do

  BASENAME=$(basename $ITEM)
  case $BASENAME in

    # Ignore the update script and the git config file.

    update.sh) ;;
    \.gitconfig) ;;

    # Copy any environment-specific files.

    *\.$SHELL_ENVIRONMENT)
      ACTUAL_NAME=$(basename -s .$SHELL_ENVIRONMENT $ITEM)
      cp $BASE_DIR/$ITEM $HOME/$ACTUAL_NAME
      ;;

    # Ignore files for other environments.

    *\.chromeos) ;;
    *\.gitbash) ;;
    *\.wsl) ;;

    # Copy everything else.

    *)
      cp $BASE_DIR/$ITEM $HOME/$ITEM
      ;;

  esac
done
