# Shell aliases.

alias ls="LC_COLLATE=C ls -F --color=auto"
alias c=clear

# Generate a random number.

alias rand="ruby -e 'puts Random.rand(100..999)'"

# Eyaml aliases for Puppet.

alias pencrypt="eyaml encrypt --quiet --output=block --pkcs7-public-key=$HOME/etc/isapps_puppet_public_key.pkcs7.pem --password"
alias fencrypt="eyaml encrypt --quiet --output=block --pkcs7-public-key=$HOME/etc/isapps_puppet_public_key.pkcs7.pem --file"

# Git aliases.

alias log="git log"
alias commit="git commit"
alias push="git push"

# Git flow aliases.

alias feature="git flow feature"
alias bugfix="git flow bugfix"
alias release="git flow release"
alias hotfix="git flow hotfix"
alias support="git flow support"

alias start="git flow feature start"
alias publish="git flow feature publish"
alias finish="git flow feature finish"
