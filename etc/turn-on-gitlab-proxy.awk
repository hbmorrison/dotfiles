BEGIN { GITLAB_SECTION = 0; }
/^Host/ { if ( GITLAB_SECTION == 1 ) { GITLAB_SECTION = 0; } }
/^Host gitlab\.is\.ed\.ac\.uk/ { GITLAB_SECTION = 1; }
/ProxyCommand none/ { if ( GITLAB_SECTION == 1 ) { next; } }
{ print; }
