BEGIN { GITLAB_SECTION = 0; GITLAB_PROXY = 1 }
/^Host/ { if ( GITLAB_SECTION == 1 ) { GITLAB_SECTION = 2; } }
/^Host gitlab\.is\.ed\.ac\.uk/ { GITLAB_SECTION = 1; }
/ProxyCommand none/ { if ( GITLAB_SECTION == 1 ) { GITLAB_PROXY = 0; next; } }
{ next; }
END { print GITLAB_PROXY; }
